# goClient

Клиентское GO-приложение. Считывает данные с CSV-файла и отправляет их на сервер через stream (для загрузки их в БД boltDB)

### Технологии: 
Go, Protobuf, gRPC, библиотека smartystreets/scanners/csv.

### Загрузка проекта
Использовать любую из команд:<br>
1 - **go get gitlab.com/oleg.kuleba/goClient** с любой директории<br>
( если не сработает, то использовать комманду go get gitlab.com/oleg.kuleba/goClient.git и после нее изменить
 название папки проекта с goClient.git на goClient )<br>
или<br>
2 - **git clone https://gitlab.com/oleg.kuleba/goClient.git** в директории %GOPATH%\src\gitlab.com\oleg.kuleba

### Использование
- Файл с именем contacts.csv должен находится в папке %GOPATH%\src\gitlab.com\oleg.kuleba\DBLoader<br>
Файл получаем по ссылке https://drive.google.com/file/d/1vvteHSBr14OLuNPENgtx4k_UegflIxB9/view?usp=sharing
- start<br>
В директории %GOPATH%\src\gitlab.com\oleg.kuleba\goClient выполнить команду go run main.go<br>
- stop<br>
Остановка автоматическая после достижения окончания файла<br>
Если нужна экстренная остановка - использовать сочетание клавиш Ctrl+C<br>
