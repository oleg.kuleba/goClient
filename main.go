package main

import (
	"google.golang.org/grpc"
	"log"
	pb "gitlab.com/oleg.kuleba/goClient/protobuf"
	"context"
	"fmt"
	"github.com/smartystreets/scanners/csv"
	"os"
	"io"
)

func main() {

	conn, err := grpc.Dial("127.0.0.1:8282", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("grpc.Dial() Error: %v", err)
	}
	defer conn.Close()

	client := pb.NewAddressBookClient(conn)

	//msg := RunSending(client)
	//log.Println(msg)
	msg := readCsvAndSend(client)
	fmt.Println(msg)
}

//func RunSending(client pb.AddressBookClient) pb.Response {
//
//	stream, err := client.SendBook(context.Background())
//	if err != nil {
//		log.Fatalf("client.SendBook() Error: %v", err)
//	}
//
//	for {
//		log.Println()
//	}
//
//	return pb.Response{
//		Message: "",
//	}
//}

type ContactScanner struct {
	*csv.Scanner
}

func NewContactScanner(reader io.Reader) *ContactScanner {
	inner := csv.NewScanner(reader)
	inner.Scan() // skip the header!
	return &ContactScanner{Scanner: inner}
}

func (this *ContactScanner) Record() (*pb.Request) {
	fields := this.Scanner.Record()
	return &pb.Request{
		Phone: &pb.Phone{
			Number: fields[0],
		},
		Contact: &pb.Contact{
			Name:  fields[1],
			Address: &pb.Contact_Address{
				City:      fields[2],
				Street:    fields[3],
				Building:  fields[4],
				Apartment: fields[5],
			},
		},
	}
}

func readCsvAndSend(client pb.AddressBookClient) string {
	file, err := os.OpenFile("../DBLoader/contacts.csv", os.O_RDONLY, os.ModePerm)
	if err != nil {
		log.Fatalf("os.OpenFile() Error: %v", err)
	}

	stream, err := client.SendBook(context.Background())
	if err != nil {
		log.Fatalf("client.SendBook() Error: %v", err)
	}

	scanner := NewContactScanner(file)

	request := &pb.Request{}
	fmt.Println("Sending data to server was started")
	//counter := 0
	for scanner.Scan() {
		request = scanner.Record()
		//fmt.Println(request.Phone)
		//fmt.Println(request.Contact)
		if err := stream.Send(request); err != nil {
			log.Fatalf("stream.Send(request) Error: %v", err)
		}

		// FOR TEST
		//if counter > 0 && counter%5000001 == 0 {
		//	break
		//}
		//counter++
	}

	if err = scanner.Error(); err != nil {
		log.Fatalf("scanner.Error() Error: %v", err)
	}

	response, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("stream.CloseAndRecv() Error: %v", err)
	}
	return response.Message
}
