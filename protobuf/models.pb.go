// Code generated by protoc-gen-go. DO NOT EDIT.
// source: protobuf/models.proto

/*
Package protobuf is a generated protocol buffer package.

It is generated from these files:
	protobuf/models.proto

It has these top-level messages:
	Phone
	Contact
	Request
	Response
*/
package protobuf

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type Phone struct {
	Number string `protobuf:"bytes,1,opt,name=number" json:"number,omitempty"`
}

func (m *Phone) Reset()                    { *m = Phone{} }
func (m *Phone) String() string            { return proto.CompactTextString(m) }
func (*Phone) ProtoMessage()               {}
func (*Phone) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Phone) GetNumber() string {
	if m != nil {
		return m.Number
	}
	return ""
}

type Contact struct {
	Name    string           `protobuf:"bytes,1,opt,name=name" json:"name,omitempty"`
	Address *Contact_Address `protobuf:"bytes,2,opt,name=address" json:"address,omitempty"`
}

func (m *Contact) Reset()                    { *m = Contact{} }
func (m *Contact) String() string            { return proto.CompactTextString(m) }
func (*Contact) ProtoMessage()               {}
func (*Contact) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *Contact) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Contact) GetAddress() *Contact_Address {
	if m != nil {
		return m.Address
	}
	return nil
}

type Contact_Address struct {
	City      string `protobuf:"bytes,1,opt,name=City" json:"City,omitempty"`
	Street    string `protobuf:"bytes,2,opt,name=Street" json:"Street,omitempty"`
	Building  string `protobuf:"bytes,3,opt,name=Building" json:"Building,omitempty"`
	Apartment string `protobuf:"bytes,4,opt,name=Apartment" json:"Apartment,omitempty"`
}

func (m *Contact_Address) Reset()                    { *m = Contact_Address{} }
func (m *Contact_Address) String() string            { return proto.CompactTextString(m) }
func (*Contact_Address) ProtoMessage()               {}
func (*Contact_Address) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1, 0} }

func (m *Contact_Address) GetCity() string {
	if m != nil {
		return m.City
	}
	return ""
}

func (m *Contact_Address) GetStreet() string {
	if m != nil {
		return m.Street
	}
	return ""
}

func (m *Contact_Address) GetBuilding() string {
	if m != nil {
		return m.Building
	}
	return ""
}

func (m *Contact_Address) GetApartment() string {
	if m != nil {
		return m.Apartment
	}
	return ""
}

type Request struct {
	Phone   *Phone   `protobuf:"bytes,1,opt,name=phone" json:"phone,omitempty"`
	Contact *Contact `protobuf:"bytes,2,opt,name=contact" json:"contact,omitempty"`
}

func (m *Request) Reset()                    { *m = Request{} }
func (m *Request) String() string            { return proto.CompactTextString(m) }
func (*Request) ProtoMessage()               {}
func (*Request) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *Request) GetPhone() *Phone {
	if m != nil {
		return m.Phone
	}
	return nil
}

func (m *Request) GetContact() *Contact {
	if m != nil {
		return m.Contact
	}
	return nil
}

type Response struct {
	Message string `protobuf:"bytes,1,opt,name=message" json:"message,omitempty"`
}

func (m *Response) Reset()                    { *m = Response{} }
func (m *Response) String() string            { return proto.CompactTextString(m) }
func (*Response) ProtoMessage()               {}
func (*Response) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *Response) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func init() {
	proto.RegisterType((*Phone)(nil), "protobuf.Phone")
	proto.RegisterType((*Contact)(nil), "protobuf.Contact")
	proto.RegisterType((*Contact_Address)(nil), "protobuf.Contact.Address")
	proto.RegisterType((*Request)(nil), "protobuf.Request")
	proto.RegisterType((*Response)(nil), "protobuf.Response")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for AddressBook service

type AddressBookClient interface {
	SendBook(ctx context.Context, opts ...grpc.CallOption) (AddressBook_SendBookClient, error)
}

type addressBookClient struct {
	cc *grpc.ClientConn
}

func NewAddressBookClient(cc *grpc.ClientConn) AddressBookClient {
	return &addressBookClient{cc}
}

func (c *addressBookClient) SendBook(ctx context.Context, opts ...grpc.CallOption) (AddressBook_SendBookClient, error) {
	stream, err := grpc.NewClientStream(ctx, &_AddressBook_serviceDesc.Streams[0], c.cc, "/protobuf.AddressBook/SendBook", opts...)
	if err != nil {
		return nil, err
	}
	x := &addressBookSendBookClient{stream}
	return x, nil
}

type AddressBook_SendBookClient interface {
	Send(*Request) error
	CloseAndRecv() (*Response, error)
	grpc.ClientStream
}

type addressBookSendBookClient struct {
	grpc.ClientStream
}

func (x *addressBookSendBookClient) Send(m *Request) error {
	return x.ClientStream.SendMsg(m)
}

func (x *addressBookSendBookClient) CloseAndRecv() (*Response, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(Response)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// Server API for AddressBook service

type AddressBookServer interface {
	SendBook(AddressBook_SendBookServer) error
}

func RegisterAddressBookServer(s *grpc.Server, srv AddressBookServer) {
	s.RegisterService(&_AddressBook_serviceDesc, srv)
}

func _AddressBook_SendBook_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(AddressBookServer).SendBook(&addressBookSendBookServer{stream})
}

type AddressBook_SendBookServer interface {
	SendAndClose(*Response) error
	Recv() (*Request, error)
	grpc.ServerStream
}

type addressBookSendBookServer struct {
	grpc.ServerStream
}

func (x *addressBookSendBookServer) SendAndClose(m *Response) error {
	return x.ServerStream.SendMsg(m)
}

func (x *addressBookSendBookServer) Recv() (*Request, error) {
	m := new(Request)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

var _AddressBook_serviceDesc = grpc.ServiceDesc{
	ServiceName: "protobuf.AddressBook",
	HandlerType: (*AddressBookServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "SendBook",
			Handler:       _AddressBook_SendBook_Handler,
			ClientStreams: true,
		},
	},
	Metadata: "protobuf/models.proto",
}

func init() { proto.RegisterFile("protobuf/models.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 291 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x64, 0x50, 0xd1, 0x4a, 0xc3, 0x40,
	0x10, 0x34, 0xda, 0xf6, 0x92, 0xcd, 0x83, 0xb8, 0xa0, 0xc4, 0x20, 0x58, 0x82, 0x42, 0x40, 0x88,
	0x90, 0xe2, 0x07, 0xb4, 0xf5, 0x03, 0xe4, 0xfa, 0xec, 0x43, 0xd2, 0xac, 0x35, 0xd8, 0xdc, 0xc5,
	0xdc, 0xe5, 0xc1, 0xef, 0xf3, 0xc7, 0x24, 0x97, 0x3b, 0x5b, 0xe8, 0xdb, 0xce, 0xec, 0xdc, 0xdc,
	0xcc, 0xc2, 0x75, 0xdb, 0x49, 0x2d, 0xcb, 0xfe, 0xe3, 0xb9, 0x91, 0x15, 0xed, 0x55, 0x66, 0x30,
	0xfa, 0x8e, 0x4e, 0xee, 0x61, 0xfa, 0xf6, 0x29, 0x05, 0xe1, 0x0d, 0xcc, 0x44, 0xdf, 0x94, 0xd4,
	0x45, 0xde, 0xdc, 0x4b, 0x03, 0x6e, 0x51, 0xf2, 0xeb, 0x01, 0x5b, 0x4b, 0xa1, 0x8b, 0xad, 0x46,
	0x84, 0x89, 0x28, 0x1a, 0xb2, 0x0a, 0x33, 0xe3, 0x02, 0x58, 0x51, 0x55, 0x1d, 0x29, 0x15, 0x9d,
	0xcf, 0xbd, 0x34, 0xcc, 0x6f, 0x33, 0x67, 0x9e, 0xd9, 0x77, 0xd9, 0x72, 0x14, 0x70, 0xa7, 0x8c,
	0x25, 0x30, 0xcb, 0x0d, 0x9e, 0xeb, 0x5a, 0xff, 0x38, 0xcf, 0x61, 0x1e, 0xb2, 0x6c, 0x74, 0x47,
	0xa4, 0x8d, 0x65, 0xc0, 0x2d, 0xc2, 0x18, 0xfc, 0x55, 0x5f, 0xef, 0xab, 0x5a, 0xec, 0xa2, 0x0b,
	0xb3, 0xf9, 0xc7, 0x78, 0x07, 0xc1, 0xb2, 0x2d, 0x3a, 0xdd, 0x90, 0xd0, 0xd1, 0xc4, 0x2c, 0x0f,
	0x44, 0xf2, 0x0e, 0x8c, 0xd3, 0x77, 0x4f, 0x4a, 0xe3, 0x23, 0x4c, 0xdb, 0xa1, 0xb1, 0xf9, 0x31,
	0xcc, 0x2f, 0x0f, 0x71, 0xcd, 0x21, 0xf8, 0xb8, 0xc5, 0x27, 0x60, 0xdb, 0x31, 0xbe, 0xed, 0x75,
	0x75, 0xd2, 0x8b, 0x3b, 0x45, 0xf2, 0x00, 0x3e, 0x27, 0xd5, 0x4a, 0xa1, 0x08, 0x23, 0x60, 0x0d,
	0x29, 0x55, 0xec, 0xdc, 0x9d, 0x1c, 0xcc, 0x5f, 0x21, 0xb4, 0xad, 0x57, 0x52, 0x7e, 0xe1, 0x0b,
	0xf8, 0x1b, 0x12, 0x95, 0x99, 0x8f, 0xcc, 0x6d, 0xce, 0x18, 0x8f, 0xa9, 0xd1, 0x3b, 0x39, 0x4b,
	0xbd, 0x72, 0x66, 0xe8, 0xc5, 0x5f, 0x00, 0x00, 0x00, 0xff, 0xff, 0xac, 0x2f, 0xf4, 0x5c, 0xdb,
	0x01, 0x00, 0x00,
}
